﻿using System.Collections.Generic;
using UnityEngine;
using Create;
using UnityEngine.UI;

namespace Colect {
    public class ColectController : MonoBehaviour
    {
        List<Box> box;

        [SerializeField]
        FishingRod fishingRod;
        [SerializeField]
        Transform FishDad;
        List<Fish> fish;

        [SerializeField]
        GameObject popUp;
        [SerializeField]
        Text text, fishText;

        [SerializeField]
        Image boxImage;

        [SerializeField]
        Sprite[] boxesImages;

        int numFish = -1;

        public static int numFishing = 4;

        void StartFishing()
        {
            numFish = -1;
            //numFishing--;
            popUp.SetActive(false);
            fishingRod.gameObject.SetActive(false);
            fishingRod.ResetRod();

            #region Box
            //raffle a boxes
            int randomBoxes = Random.Range(0, GameController.currentLevel.boxes.Length);
            Boxes boxes = GameController.currentLevel.boxes[randomBoxes];

            List<Box> c = new List<Box>();
            for (int i = 0; i < boxes.box.Length; i++)
            {
                c.Add(boxes.box[i]);
            }

            box = new List<Box>();
            int count = c.Count;
            for (int i = 0; i < count; i++)
            {
                int randomContets = Random.Range(0, c.Count);
                box.Add(c[randomContets]);
                c.Remove(c[randomContets]);
            }
            #endregion

            #region Fish
            if (fish == null)
            {
                InitiateFish();
            }

            EnableFish(true);

            List<Fish> f = fish;

            int halfContents = f.Count/ box.Count;

            for (int j = 0; j < box.Count; j++)
            {
                for (int i = 0; i < halfContents; i++)
                {
                    int randomFish = Random.Range(0, f.Count);
                    f[randomFish].numBox = j + 1;
                    f[randomFish].colectController = this;
                    f.Remove(f[randomFish]);
                }
            }
            #endregion
        }

        void Start()
        {
            if (fish == null)
            {
                InitiateFish();
            }
        }

        void InitiateFish()
        {
            fish = new List<Fish>();
            for (int i = 0; i < FishDad.childCount; i++)
            {
                if (FishDad.GetChild(i).GetComponent<Fish>() != null)
                {
                    fish.Add(FishDad.GetChild(i).GetComponent<Fish>());
                }
            }
        }

        void EnableFish(bool b)
        {
            for (int i = 0; i < fish.Count; i++)
            {
                fish[i].gameObject.SetActive(b);
            }
        }

        public void ActivePopUp(int num)
        {
            EnableFish(false);
            numFish = num - 1;
            text.text = box[numFish].ListOfIngredients;
            fishText.text = num.ToString();
            boxImage.sprite = boxesImages[numFish];
            popUp.SetActive(true);
        }

        public void SendIngredients()
        {
            popUp.SetActive(false);
            text.text = "";

            Content[] contents = box[numFish].contents;
            for (int i = 0; i < contents.Length; i++)
            {
                Content content = contents[i];
                for (int j = 0; j < content.Amount; j++) {
                    GameController.instance.SendIngredient(content.ingredient);
                }
            }
            ControlScreens.instance.ChangeScreen(ControlScreens.Screens.Game);
        }
    }
}

