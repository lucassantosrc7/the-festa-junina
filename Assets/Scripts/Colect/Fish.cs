﻿using UnityEngine;

namespace Colect
{
    public class Fish : MonoBehaviour
    {
        [HideInInspector]
        public int numBox = 0;

        [HideInInspector]
        public ColectController colectController;

        void OnTriggerEnter2D(Collider2D hit)
        {
            colectController.ActivePopUp(numBox);
        }
    }
}

