﻿using UnityEngine;

namespace Colect
{
    [RequireComponent(typeof(EventTriggerFunctions))]
    public class FishingRod : MonoBehaviour
    {
        BoxCollider2D boxCollider;

        EventTriggerFunctions eventTrigger;

        RectTransform rectTransform;

        void Start()
        {
            eventTrigger = GetComponent<EventTriggerFunctions>();
            boxCollider = GetComponentInChildren<BoxCollider2D>();
            rectTransform = GetComponent<RectTransform>();

            eventTrigger.function = EventTriggerFunctions.Functions.Null;
            boxCollider.isTrigger = true;
            boxCollider.enabled = false;
        }

        public void ResetRod()
        {
            if(rectTransform == null)
            {
                rectTransform = GetComponent<RectTransform>();
            }
            if(boxCollider == null)
            {
                boxCollider = GetComponentInChildren<BoxCollider2D>();
            }
            if (eventTrigger == null)
            {
                eventTrigger = GetComponent<EventTriggerFunctions>();
            }

            eventTrigger.function = EventTriggerFunctions.Functions.Null;
            rectTransform.anchoredPosition = Vector2.zero;
            boxCollider.enabled = false;
            gameObject.SetActive(true);
        }

        void Update()
        {
            if (eventTrigger.function == EventTriggerFunctions.Functions.Down)
            {
                boxCollider.enabled = false;

                #region Move
                Vector2 pos;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(
                                     GameController.canvas.transform as RectTransform,
                                     Input.mousePosition,
                                     GameController.canvas.worldCamera, out pos);

                transform.position = GameController.canvas.transform.TransformPoint(pos);
                #endregion
            }
            else if (eventTrigger.function == EventTriggerFunctions.Functions.Up)
            {
                eventTrigger.function = EventTriggerFunctions.Functions.Null;
                boxCollider.enabled = true;
            }
        }
    }
}

