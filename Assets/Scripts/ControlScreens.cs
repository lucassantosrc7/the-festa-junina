﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlScreens : MonoBehaviour {

    public static ControlScreens instance { get; private set; }

    public enum Screens {
        Game,Menu, SelectLevel, SelectChar, Kitchen, Colect, Load, Options, Credits
    }
    [SerializeField]
    Screens first_Screen;

    public static Screens currentScreen;

    [Header("Screens")]
    public GameObject menu;
    public GameObject game, selectLevel ,selectChar, kitchen, colect, load, 
                      options, credits;

    void Start () {
        instance = this;

        ChangeScreen(first_Screen);
    }

    public void ChangeScreen(Screens newScreen)
    {
        switch (currentScreen)
        {
            case Screens.Menu:
                menu.SetActive(false);
                break;
            case Screens.Game:
                game.SetActive(false);
                break;
            case Screens.SelectChar:
                selectChar.SetActive(false);
                break;
            case Screens.SelectLevel:
                selectLevel.SetActive(false);
                break;
            case Screens.Kitchen:
                kitchen.SetActive(false);
                break;
            case Screens.Colect:
                colect.SetActive(false);
                break;
            case Screens.Load:
                load.SetActive(false);
                break;
            case Screens.Options:
                options.SetActive(false);
                break;
            case Screens.Credits:
                credits.SetActive(false);
                break;
        }

        switch (newScreen)
        {
            case Screens.Menu:
                menu.SetActive(true);
                SoundController.ChangeAmbience("Menu");
                break;
            case Screens.Game:
                game.SetActive(true);
                SendMessage("StartGame");
                break;
            case Screens.SelectChar:
                selectChar.SetActive(true);
                selectChar.SendMessage("CheckChars");
                break;
            case Screens.SelectLevel:
                selectLevel.SetActive(true);
                SendMessage("IniLevel");
                break;
            case Screens.Kitchen:
                kitchen.SetActive(true);
                kitchen.SendMessage("StartKitchen");
                break;
            case Screens.Colect:
                colect.SetActive(true);
                colect.SendMessage("StartFishing");
                break;
            case Screens.Load:
                load.SetActive(true);
                break;
            case Screens.Options:
                options.SetActive(true);
                break;
            case Screens.Credits:
                credits.SetActive(true);
                break;
        }

        currentScreen = newScreen;
    }
}
