﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Create
{
    [CreateAssetMenu(fileName = "Alternative Boxes", menuName = "Create/Alternative Boxes", order = 4)]
    public class Boxes : ScriptableObject
    {
        public Box[] box;
    }

    [System.Serializable]
    public class Box
    {
        public Content [] contents;

        [TextArea]
        public string ListOfIngredients;
    }

    [System.Serializable]
    public class Content
    {
        public Ingredient ingredient;
        public int Amount;
    }
}
