﻿using System.Collections;
using UnityEngine;

namespace Create
{
    [CreateAssetMenu(fileName = "Ingredient", menuName = "Create/Ingredient", order = 2)]
    public class Ingredient : ScriptableObject
    {
        public string ingredient;
        [Space(5)]
        public UnityEngine.UI.Image image;
        public Recipe.IngredientName ingredientName;
        public int AmountRecipes = 1;
        [Space(5)]
        //public GameObject toColect;
        public GameObject toUse;
        public GameObject toTrash;
    }
}
