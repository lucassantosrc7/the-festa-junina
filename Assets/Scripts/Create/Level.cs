﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Create
{
    [CreateAssetMenu(fileName = "Level", menuName = "Create/Level", order = 3)]
    public class Level : ScriptableObject
    {
        public LevelConfig[] recipes;

        public Boxes[] boxes;

        public LevelConfig reward;
        public GameObject imageReward;
    }

    [System.Serializable]
    public class LevelConfig
    {
        public string recipeName;

        public Recipe recipe;
        public int amount;
    }
}
