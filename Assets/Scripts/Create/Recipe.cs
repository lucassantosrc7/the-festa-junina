﻿using System.Collections;
using UnityEngine;

namespace Create
{
    [CreateAssetMenu(fileName = "Recipe", menuName = "Create/Recipe", order = 1)]
    public class Recipe : ScriptableObject
    {
        public enum IngredientName {
            Oil, Popcorn, Milk, Cinnamon, Sugar, Corn, Water, Coco
        }
        public IngredientConfig[] ingredients;
        
        public GameObject recipeList;
    }

    [System.Serializable]
    public class IngredientConfig
    {
        public Ingredient ingredient;
        public int Amount = 1;
    }
}
