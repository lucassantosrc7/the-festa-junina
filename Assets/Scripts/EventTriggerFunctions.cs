﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EventTriggerFunctions : EventTrigger
{
    public enum Functions
    {
        Null, Down, Up
    }
    [HideInInspector]
    public Functions function = Functions.Null;

    [HideInInspector]
    public PointerEventData eventData = null;

    public override void OnPointerUp(PointerEventData data)
    {
        function = Functions.Up;
    }
    public override void OnPointerDown(PointerEventData data)
    {
        eventData = data;
        function = Functions.Down;
    }
}

