﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class FeedBackPauDSebo : MonoBehaviour
    {
        Slider[] sliders;

        bool startAdd = false;

        [SerializeField]
        int quantityPerBag;

        [SerializeField]
        float quantityPerSecond;

        [SerializeField]
        Text text;

        int current_Bag;
        float amount = 10;
        PauDeSebo pauDeSebo;

        float numBag, leftover;
        int wins;

        public void StartFeedBack(float A, PauDeSebo PS)
        {
            if (sliders == null)
            {
                sliders = GetComponentsInChildren<Slider>();

                for(int i = 0; i < sliders.Length; i++)
                {
                    sliders[i].maxValue = quantityPerBag;
                }
            }

            A += leftover;

            wins = 0;
            current_Bag = 0;
            amount = A;
            pauDeSebo = PS;

            leftover = amount % quantityPerBag;
            if (amount > quantityPerBag)
            {
                numBag = amount / quantityPerBag;
                numBag -= leftover / quantityPerBag;
            }
            else
            {
                numBag = 0;
            }

            text.gameObject.SetActive(false);
            startAdd = true;
        }

        void Start()
        {
            if(sliders == null)
            {
                sliders = GetComponentsInChildren<Slider>();
                for (int i = 0; i < sliders.Length; i++)
                {
                    sliders[i].maxValue = quantityPerBag;
                }
            }
        }

        void Update()
        {
            if (startAdd)
            {
                if (current_Bag < sliders.Length)
                {
                    if (current_Bag < numBag)
                    {
                        if (sliders[current_Bag].value >= sliders[current_Bag].maxValue)
                        {
                            current_Bag++;
                            wins++;
                        }
                        else
                        {
                            sliders[current_Bag].value += quantityPerSecond * Time.deltaTime;
                        }
                    }
                    else 
                    {
                        if (sliders[current_Bag].value < leftover)
                        {
                            sliders[current_Bag].value += quantityPerSecond * Time.deltaTime;
                        }
                        else
                        {
                            sliders[current_Bag].value = leftover;

                            text.gameObject.SetActive(true);
                            text.text= "Parabéns você ganhou " + wins.ToString() + " saquinhos de pipoca";

                            StartCoroutine(ShowResult(1.8f));
                            startAdd = false;
                        }
                    }
                }
                else
                {
                    text.gameObject.SetActive(true);
                    text.text = "Parabéns você ganhou " + wins.ToString() + " saquinhos de pipoca";

                    StartCoroutine(ShowResult(1.8f));
                    startAdd = false;
                }
            } 
        }

        IEnumerator ShowResult(float t)
        {
            yield return new WaitForSeconds(t);
            pauDeSebo.FinishMiniGame(wins);
            for (int i = 0; i < sliders.Length; i++)
            {
                sliders[i].value = 0;
            }
            text.gameObject.SetActive(false);
        }

    }
}
