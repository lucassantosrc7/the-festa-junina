﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TextFeedBack : MonoBehaviour
{
    Text text;

    [SerializeField]
    float time;
    [SerializeField]
    float numMaxDots = 3;

    float currentTime;
    float currentDot;

    string initext;

    void Start()
    {
        text = GetComponent<Text>();
        initext = text.text;
    }

    void Update()
    {
        if(Time.time > currentTime)
        {
            currentDot++;
            if(currentDot > numMaxDots)
            {
                currentDot = 0;
            }

            string currentText = initext;
            for(int i = 0; i < currentDot; i++)
            {
                currentText = currentText + ".";
            }
            text.text = currentText;
            currentTime = time + Time.time;
        }
    }
}
