﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveKitchenPopUp : MonoBehaviour
{
    [SerializeField]
    string namePopUp;

    [SerializeField]
    float timeToFalse;

    void initialize()
    {
        GameController.instance.kitchenController.ActivePopup(namePopUp, timeToFalse);
    }
}
