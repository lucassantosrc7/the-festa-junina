﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class KitchenController : MonoBehaviour
    {
        public PauDeSebo pauDeSebo;

        public GameObject [] popsUp;

        public GameObject putIngredients;

        void StartKitchen()
        {
            pauDeSebo.gameObject.SetActive(false);

            ActivePopup("", 0);
        }

        public void ActivePopup(string pName, float timeToFalse)
        {
            for(int i=0; i < popsUp.Length; i++)
            {
                if(popsUp[i].name == pName)
                {
                    popsUp[i].SetActive(true);
                    if(timeToFalse > 0)
                    {
                        StartCoroutine(DesactivePopup(popsUp[i], timeToFalse));
                    }
                }
                else
                {
                    popsUp[i].SetActive(false);
                }
            }
        }

        IEnumerator DesactivePopup(GameObject popup, float t)
        {
            yield return new WaitForSeconds(t);
            popup.SetActive(false);

            RecipeController.RecipeIsOver(1);
            ControlScreens.instance.ChangeScreen(ControlScreens.Screens.Game);
            gameObject.SetActive(false);
        }
    }
}