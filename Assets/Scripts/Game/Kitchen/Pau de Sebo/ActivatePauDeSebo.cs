﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Create;

namespace Game
{
    [RequireComponent(typeof(UseIngredient))]
    public class ActivatePauDeSebo : MonoBehaviour
    {
        UseIngredient useIngredient;

        [SerializeField]
        float Max_Amount, Min_Amount;

        [SerializeField]
        PauDeSebo.TypesOfPau typesOfPau = PauDeSebo.TypesOfPau.Null;

        void initialize()
        {
            if (useIngredient == null)
            {
                useIngredient = GetComponent<UseIngredient>();
            }

            if (useIngredient.finish)
            {
                GameController.instance.kitchenController.pauDeSebo.StartMiniGame(typesOfPau,Max_Amount,Min_Amount);
            }
        }
    }
}

