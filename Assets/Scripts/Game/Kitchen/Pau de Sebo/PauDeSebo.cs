﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    [RequireComponent(typeof(Slider))]
    public class PauDeSebo : MonoBehaviour
    {
        public enum TypesOfPau
        {
            Null, Popcorn
        }

        Slider slider;

        [SerializeField]
        GameObject tutorialPopup;

        [Header("Value")]
        [SerializeField]
        float max;
        [SerializeField]
        float decrese, maxIncrese, minIncrese;

        bool showResult = false, startclick = false;

        [Space(20)]
        [SerializeField]
        float time;

        [Header("Type of pau")]
        [SerializeField]
        TypeOfPau[] typesOfPaus;
        TypeOfPau typeOfPau;

        Image fill;

        float minFill, maxFill;
        int numFill;

        float maxAmount, minAmount;

        public void StartMiniGame(TypesOfPau TP, float MaxA, float MinA)
        {
            gameObject.SetActive(true);

            startclick = false;
            showResult = false;
            tutorialPopup.SetActive(true);

            if (slider == null)
            {
                slider = GetComponent<Slider>();
            }
            if (fill == null)
            {
                fill = slider.handleRect.GetComponentInChildren<Image>();
            }

            slider.maxValue = max;
            slider.value = 0;

            typeOfPau = SelectTypeOfPau(TP);

            maxAmount = MaxA;
            minAmount = MinA;
        }

        void Awake()
        {
            if (slider == null)
            {
                slider = GetComponent<Slider>();
            }
            if (fill == null)
            {
                fill = slider.handleRect.GetComponentInChildren<Image>();
            }

            for(int i = 0; i < typesOfPaus.Length; i++)
            {
                if(typesOfPaus[i].popup != null)
                {
                    typesOfPaus[i].popup.gameObject.SetActive(false);
                }
            }
        }

        void Update()
        {
            if (!showResult && startclick)
            {
                slider.value = Mathf.Clamp(slider.value - decrese * Time.deltaTime, 0, max);

                /*for(int i = 0; i < Input.touchCount; i++)
                {
                    if (Input.GetTouch(i).phase == TouchPhase.Began || Input.GetMouseButtonDown(0))
                    {
                        float x = Mathf.InverseLerp(0, max, slider.value);
                        slider.value += Mathf.Lerp(maxIncrese, minIncrese, x);
                    }
                }*/

                if (Input.GetMouseButtonDown(0))
                {
                    float x = Mathf.InverseLerp(0, max, slider.value);
                    float increase = Mathf.Lerp(maxIncrese, minIncrese, x);
                    slider.value += increase;
                }

                if (slider.value <= minFill)
                {
                    numFill--;
                    ChangeFill();
                }
                else if (slider.value >= maxFill)
                {
                    numFill++;
                    ChangeFill();
                }
            }
            else if(!startclick && Input.GetMouseButtonDown(0))
            {
                startclick = true;
                tutorialPopup.SetActive(false);
                StartCoroutine(CallFeedback(time));
            }
        }

        TypeOfPau SelectTypeOfPau(TypesOfPau TP)
        {
            for (int i = 0; i < typesOfPaus.Length; i++)
            {
                if (TP == typesOfPaus[i].typeOfPau)
                {
                    return typesOfPaus[i];
                }
            }
            return typesOfPaus[0];
        }

        void ChangeFill()
        {
            numFill = Mathf.Clamp(numFill, 0, typeOfPau.fills.Length - 1);
            fill.sprite = typeOfPau.fills[numFill];

            maxFill = (max / typeOfPau.fills.Length) * numFill;
            minFill = (max / typeOfPau.fills.Length) * (numFill - 1);

            maxFill = Mathf.Clamp(maxFill, 0, max);
            minFill = Mathf.Clamp(minFill, 0, max);
        }

        IEnumerator CallFeedback(float t)
        {
            yield return new WaitForSeconds(t);
            if(typeOfPau.popup != null)
            {
                showResult = true;

                float SV = Mathf.InverseLerp(0, max, slider.value);
                float v = Mathf.Lerp(minAmount, maxAmount, SV);

                typeOfPau.popup.gameObject.SetActive(true);
                object[] sendValues = new object[2] { v, this };
                typeOfPau.popup.StartFeedBack(v, this);
            }
            else
            {
                FinishMiniGame(0);
            }
        }

        public void FinishMiniGame(int Amount)
        {
            showResult = false;
            if (typeOfPau.popup != null)
            {
                typeOfPau.popup.gameObject.SetActive(false);
            }

            RecipeController.RecipeIsOver(Amount);
            ControlScreens.instance.ChangeScreen(ControlScreens.Screens.Game);
            gameObject.SetActive(false);
        }
    }

    [System.Serializable]
    public class TypeOfPau
    {
        public PauDeSebo.TypesOfPau typeOfPau = PauDeSebo.TypesOfPau.Null;
        public Sprite[] fills;
        public int QuantPerAmount = 0;
        public FeedBackPauDSebo popup;
    }
}
