﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAvatar : MonoBehaviour
{
    static Image image;

    [SerializeField]
    PlayerConfig[] playerConfigs;
    static PlayerConfig [] players;

    void Awake()
    {
        image = GetComponent<Image>();
        players = playerConfigs;
    }
    void Update()
    {
        
    }

    public static void CheckPlayer()
    {
        for(int i = 0; i < players.Length; i++)
        {
            if(players[i].characters == GameController.myCharacter)
            {
                image.sprite = players[i].image;
            }
        }
    }
}

[System.Serializable]
public class PlayerConfig
{
    public Sprite image;
    public GameController.Characters characters;
}
