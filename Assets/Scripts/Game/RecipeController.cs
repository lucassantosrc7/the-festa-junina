﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Create;
using Game;

public class RecipeController : MonoBehaviour
{
    static List<Recipe> probablyRecipes;
    static int currentIngredient = 0;

    static Recipe CurrentRecipe;

    public static bool usingIngredient = false;

    public static void CheckRecipe(Slots slot)
    {
        if (usingIngredient)
        {
            return;
        }

        #region CheckTheIngredient
        if (currentIngredient > 0)
        {
            if(CurrentRecipe != null)
            {
                if (CurrentRecipe.ingredients[currentIngredient].ingredient.ingredientName != slot.ingredient.ingredientName)
                {
                    currentIngredient = 0;
                    slot.LoseFood();
                    CurrentRecipe = null;
                    return;
                }
            }
            else
            {
                for (int i = 0; i < probablyRecipes.Count; i++)
                {
                    if (probablyRecipes[i].ingredients[currentIngredient].ingredient.ingredientName != slot.ingredient.ingredientName)
                    {
                        probablyRecipes.Remove(probablyRecipes[i]);
                    }
                }

                if (!CheckProbablyRecipes())
                {
                    currentIngredient = 0;
                    slot.LoseFood();
                    return;
                }
            }
        }
        else
        {
            Recipe[] recipes = new Recipe[GameController.currentLevel.recipes.Length];
            probablyRecipes = new List<Recipe>();

            for (int i = 0; i < recipes.Length; i++)
            {
                recipes[i] = GameController.currentLevel.recipes[i].recipe;
            }

            for (int i = 0; i < recipes.Length; i++)
            {
                if (recipes[i].ingredients[currentIngredient].ingredient.ingredientName == slot.ingredient.ingredientName)
                {
                    probablyRecipes.Add(recipes[i]);
                }
            }

            if (!CheckProbablyRecipes())
            {
                currentIngredient = 0;
                slot.LoseFood();
                return;
            }
        }
        #endregion

        usingIngredient = true;
        GameController.instance.UpdateHud(null);

        UseIngredient(slot.toUse);
        slot.RightFood();
    }

    static void UseIngredient(GameObject toUse)
    {
        ControlScreens.instance.ChangeScreen(ControlScreens.Screens.Kitchen);
        //Use
        toUse.SetActive(true);
        toUse.SendMessage("Use", SendMessageOptions.DontRequireReceiver);
    }

    public static void RecipeIsOver(int amount)
    {
        currentIngredient++;
        usingIngredient = false;

        if (CurrentRecipe != null)
        {
            if(currentIngredient < CurrentRecipe.ingredients.Length)
            {
                GameController.instance.UpdateHud(CurrentRecipe.ingredients[currentIngredient].ingredient);
                return;
            }
        }
        else
        {
            for (int i = 0; i < probablyRecipes.Count; i++)
            {
                if (currentIngredient < probablyRecipes[i].ingredients.Length)
                {
                    GameController.instance.UpdateHud(probablyRecipes[i].ingredients[currentIngredient].ingredient);
                    return;
                }
            }
            return;
        }

        GameController.instance.RecipeFinish(CurrentRecipe, amount);
        GameController.instance.UpdateHud(null);
        CurrentRecipe = null;
        currentIngredient = 0;
        probablyRecipes = new List<Recipe>();
    }

    static bool CheckProbablyRecipes()
    {
        if (probablyRecipes.Count == 1)
        {
            CurrentRecipe = probablyRecipes[0];
            probablyRecipes = new List<Recipe>();
            return true;
        }
        else if (probablyRecipes.Count == 0)
        {
            CurrentRecipe = null;
            probablyRecipes = new List<Recipe>();
            return false;
        }

        return true;
    }
}
