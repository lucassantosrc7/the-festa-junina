﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecipeList : MonoBehaviour
{
    public static GameObject[] recipesList;

    [SerializeField]
    Image[] pages;

    int currentList = 0;
    bool next = true;

    void OnEnable()
    {
        bool page1 = true;
        for(int i = 0; i < recipesList.Length; i++)
        {
            if (page1)
            {
                recipesList[i] = Instantiate(recipesList[i], pages[0].transform);
            }
            else
            {
                recipesList[i] = Instantiate(recipesList[i], pages[1].transform);
            }

            page1 = !page1;
        }

        for (int i = 0; i < pages.Length; i++)
        {
            if(i < recipesList.Length && recipesList[i] != null)
            {
                recipesList[i].gameObject.SetActive(true);
            }
        }
    }

    public void Next()
    {
        if (next)
        {
            currentList += 2;

            CheckPage();
        }
    }

    public void Back()
    {
        currentList-= 2;

        if(currentList <= 0)
        {
            currentList = 0;
        }

        CheckPage();
    }

    void CheckPage()
    {
        next = false;
        for (int i = 0; i < recipesList.Length; i++)
        {
            recipesList[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < pages.Length; i++)
        {
            if (currentList + i < recipesList.Length)
            {
                recipesList[currentList + i].SetActive(true);
                next = true;
            }
        }
    }
}
