﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Create;

namespace Game
{
    [RequireComponent(typeof(AudioSource))]
    public class Slots : MonoBehaviour
    {
        Button button;

        [HideInInspector]
        public Ingredient ingredient;
        [HideInInspector]
        public GameObject toUse;
        [HideInInspector]
        public GameObject toTrash;
        [HideInInspector]
        public int amount;

        Recipe.IngredientName ingredientName;

        [SerializeField]
        Image slotImage;
        [SerializeField]
        Text slotAmount;

        [SerializeField]
        Vector3 loseRot, hitScale;

        [SerializeField]
        float time;
        AudioSource source;
        [SerializeField]
        AudioClip loseClip;
        void UpdateSlot()
        {
            if (source == null)
            {
                source = GetComponent<AudioSource>();
            }
            if (button == null)
            {
                button = GetComponent<Button>();
            }

            if (ingredient == null){
                UnusableSlot();
                return;
            }

            button.interactable = true;
            ingredientName = ingredient.ingredientName;
            slotImage.gameObject.SetActive(true);
            SetImage();

            if(toUse == null)
            {
                toUse = Instantiate(ingredient.toUse, Vector3.zero, Quaternion.identity, GameController.instance.kitchenController.putIngredients.transform);
                toUse.SetActive(false);
            }
            if (toTrash == null)
            {
                toTrash = Instantiate(ingredient.toTrash, Vector3.zero, Quaternion.identity, GameController.instance.trashPopup.transform);
                toTrash.SetActive(false);
            }

            CheckAmount();
        }

        public void EnablePopUp()
        {
            if (CheckAmount())
            {
                GameController.instance.foodTrafficking.ActivePopup(this);
            }
        }

        public bool CheckAmount()
        {
            bool hasAmount = false;

            if (amount >= 0)
            {
                hasAmount = true;
                slotAmount.text = amount.ToString();
            }

            if (amount <= 0)
            {
                UnusableSlot();
            }

            return hasAmount;
        }

        void SetImage()
        {
            slotImage.sprite = ingredient.image.sprite;
            slotImage.rectTransform.anchorMin = ingredient.image.rectTransform.anchorMin;
            slotImage.rectTransform.anchorMax = ingredient.image.rectTransform.anchorMax;
            slotImage.rectTransform.anchoredPosition = ingredient.image.rectTransform.anchoredPosition;
        }

        public void LoseFood(){
            iTween.ShakeRotation(gameObject, loseRot,time);
            //Handheld.Vibrate();
            if(!source.isPlaying){

                 source.PlayOneShot(loseClip,SoundController.volume);

            }
            GameController.instance.trashPopup.SetActive(true);
            if (ingredient != null)
            {
                toTrash.SetActive(true);
            }
        }
        public void RightFood(){
            SoundController.Play("HitSlot");
        }

        void UnusableSlot()
        {
            slotImage.gameObject.SetActive(false);
            button.interactable = false;

            amount = 0;
            ingredient = null;
            slotAmount.text = amount.ToString();

            GameController.instance.CheckAmounIngredients();
        }
    }
}

