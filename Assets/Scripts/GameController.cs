﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Create;
using Game;
using Colect;

[RequireComponent(typeof(Canvas))]
public class GameController : Photon.MonoBehaviour
{
    public static GameController instance { get; private set;}
    public static Canvas canvas { get; private set; }

    public enum Characters
    {
        Roxo, Vermelho, Azul, Laranja
    }

    public static Characters myCharacter;

    #region Canvas
    public RectTransform gameCanvas;
    public static int[] recipesFinish;
    #endregion

    #region Game
    public FoodTrafficking foodTrafficking;
    public KitchenController kitchenController;
    [SerializeField]
    GameObject winPopup, loserPopup, reward;
    public GameObject trashPopup;
    public Text feedbackReceiver, amountPopup;

    static Text receiverTXT;
    static GameObject receiverDad;
    #endregion

    public static Level currentLevel;

    #region Slots
    [Header("Slots")]
    [SerializeField]
    Transform slots;
    static Slots[] ingredientSlots;
    #endregion

    public static Dictionary<string, int> ingredients;
    public static Dictionary<string, int> allIngredients;
    public static bool IniIngredients = false;
    public static bool IniAllIngredients = false;

    void Awake()
    {
        instance = this;
        canvas = GetComponent<Canvas>();

        ingredientSlots = slots.GetComponentsInChildren<Slots>();
    }

    void Update()
    {
        
    }

    void StartGame()
    {
        UpdateHud(null);

        trashPopup.SetActive(false);
        winPopup.SetActive(false);
        loserPopup.SetActive(false);

        if (receiverDad == null)
        {
            receiverDad = feedbackReceiver.transform.parent.gameObject;
            receiverTXT = feedbackReceiver;
        }
        receiverDad.SetActive(false);

        if ((recipesFinish != null && recipesFinish.Length != currentLevel.recipes.Length) || recipesFinish == null){
            recipesFinish = new int[currentLevel.recipes.Length];
        }

        PlayerAvatar.CheckPlayer();

        #region AllIngredients
        if (!IniAllIngredients)
        {
            if(allIngredients == null)
            {
                allIngredients = new Dictionary<string, int>();
            }

            for (int i = 0; i < currentLevel.recipes.Length; i++)
            {
                Recipe recipe = currentLevel.recipes[i].recipe;
                for (int j = 0; j < recipe.ingredients.Length; j++)
                {
                    int a = currentLevel.recipes[i].amount * recipe.ingredients[j].Amount;
                    string ingredienteName = recipe.ingredients[j].ingredient.ingredientName.ToString();

                    if (!allIngredients.ContainsKey(ingredienteName))
                    {
                        allIngredients.Add(ingredienteName, a);
                    }
                    else
                    {
                        allIngredients[ingredienteName] += a;
                    }
                }
            }
            IniAllIngredients = true;
        }

        if (!IniIngredients)
        {
            if(ingredients == null)
            {
                ingredients = new Dictionary<string, int>();
            }
            IniIngredients = true;

            for (int i = 0; i < ingredientSlots.Length; i++)
            {
                if(ingredientSlots[i].ingredient != null)
                {
                    string iName = ingredientSlots[i].ingredient.ingredientName.ToString();
                    int a = ingredientSlots[i].amount;
                    int iA = ingredientSlots[i].ingredient.AmountRecipes;
                    photonView.RPC("AddIngredient", PhotonTargets.AllBuffered, iName, a, iA);
                }
            }
        }
        #endregion

        UpdateSlots();
    }

    public void SendIngredient(Ingredient ingredient)
    {
        if (IniIngredients)
        {
            string iName = ingredient.ingredientName.ToString();
            int a = 1;
            int iA = ingredient.AmountRecipes;
            photonView.RPC("AddIngredient", PhotonTargets.AllBuffered, iName, a, iA);
        }

        for (int i = 0; i < ingredientSlots.Length; i++)
        {
            if (ingredientSlots[i].ingredient != null && ingredientSlots[i].ingredient == ingredient)
            {
                ingredientSlots[i].amount++;
                i = ingredientSlots.Length;
            }
            else if (i >= (ingredientSlots.Length - 1) || ingredientSlots[i].ingredient == null)
            {
                ingredientSlots[i].ingredient = ingredient;
                ingredientSlots[i].amount++;
                i = ingredientSlots.Length;
            }
        }
    }

    [PunRPC]
    void AddIngredient(string ingredienteName, int a, int iA)
    {
        if (!ingredients.ContainsKey(ingredienteName))
        {
            ingredients.Add(ingredienteName, a * iA);
        }
        else
        {
            ingredients[ingredienteName] += a * iA;
        }
    }

    public void CheckAmounIngredients()
    {
        if(ColectController.numFishing > 0)
        {
            return;
        }

        /*for (int i = 0; i < currentLevel.recipes.Length; i++)
        {
            Recipe recipe = currentLevel.recipes[i].recipe;
            for (int j = 0; j < recipe.ingredients.Length; j++)
            {
                int a = currentLevel.recipes[i].amount * recipe.ingredients[j].Amount;
                string iName = recipe.ingredients[j].ingredient.ingredientName.ToString();

                if(ingredients[iName] < allIngredients[iName])
                {
                    photonView.RPC("Lost", PhotonTargets.All);
                }
            }
        }*/
    }

    [PunRPC]
    void Lost()
    {
        /*
        loserPopup.SetActive(true);
        StartCoroutine(BackToMenu(1.5f));*/
    }

    [PunRPC]
    void Win()
    {
        winPopup.SetActive(true);

        if (currentLevel.reward != null)
        {
            reward.SetActive(false);
            LevelController.porPamonha = true;

            if (currentLevel.imageReward != null)
            {
                Instantiate(currentLevel.imageReward, reward.transform);
            }

            LevelController.playerRecipes.Add(currentLevel.reward.recipe);
            StartCoroutine(ActiveReward(1.5f));
            return;
        }

        StartCoroutine(BackToMenu(1.5f));
    }

    void CheckAmounRecipe()
    {
        for (int i = 0; i < recipesFinish.Length; i++)
        {
            if (i < recipesFinish.Length && recipesFinish[i] < currentLevel.recipes[i].amount)
            {
                return;
            }
        }

        photonView.RPC("Win", PhotonTargets.All);
    }

    /*public void CheckSlots(){
        for (int i = 0; i < ingredientSlots.Length; i++){
            if(ingredientSlots[i].amount > 0){
                return;
            }
        }

        loserPopup.SetActive(true);
        StartCoroutine(BackToMenu(1.5f, loserPopup));
    }*/

    public static void UpdateSlots()
    {
        for (int i = 0; i < ingredientSlots.Length; i++)
        {
            ingredientSlots[i].SendMessage("UpdateSlot");
        }
    }

    public static void ReceivedAnIngredient(Ingredient ingredient)
    {
        receiverTXT.text = ingredient.ingredient;
        receiverDad.SetActive(true);

        instance.SendIngredient(ingredient);
        UpdateSlots();
    }

    public void ClosePopup(GameObject GO)
    {
        GO.SetActive(false);
    }
    public void OpenPopup(GameObject GO)
    {
        GO.SetActive(true);
    }

    public void RecipeFinish(Recipe recipe, int amount)
    {
        for(int i = 0; i < currentLevel.recipes.Length; i++)
        {
            if (recipe == currentLevel.recipes[i].recipe)
            {
                photonView.RPC("DecreaseRecipe", PhotonTargets.All, i, amount);

                float a = currentLevel.recipes[i].amount - recipesFinish[i];

                if (a > 0)
                {
                    amountPopup.text = "Falta " + a + " " + currentLevel.recipes[i].recipeName;
                }
                else
                {
                    amountPopup.text = "Você não precisa mais fazer " + currentLevel.recipes[i].recipeName;
                }

                amountPopup.transform.parent.gameObject.SetActive(true);
            }
        }
    }

    [PunRPC]
    void DecreaseRecipe(int numRecipe, int amount)
    {
        recipesFinish[numRecipe] += amount;

        IngredientConfig[] ingConfig = currentLevel.recipes[numRecipe].recipe.ingredients;
        for (int i = 0; i < ingConfig.Length; i++)
        {
            string IName = ingConfig[i].ingredient.ingredientName.ToString();
            allIngredients[IName] -= amount;
        }

        //Check if Win
        CheckAmounRecipe();

        //Check if Loose
        CheckAmounIngredients();
    }

    public void GoingToColect()
    {
        if(ColectController.numFishing > 0)
        {
            ControlScreens.instance.ChangeScreen(ControlScreens.Screens.Colect);
        }
    }

    public void Play()
    {
        ControlScreens.instance.ChangeScreen(ControlScreens.Screens.SelectLevel);
    }

    public void GoingToOptions()
    {
        ControlScreens.instance.ChangeScreen(ControlScreens.Screens.Options);
    }

    IEnumerator BackToMenu(float t)
    {
        float vol = SoundController.volume;
        SoundController.ChangeVolume(0);
        yield return new WaitForSeconds(t);
        SoundController.volume = vol;

        IniIngredients = false;
        IniAllIngredients = false;
        ColectController.numFishing = 4;

        PhotonNetwork.LeaveRoom();
    }

    IEnumerator ActiveReward(float t)
    {
        yield return new WaitForSeconds(t);
        winPopup.SetActive(false);
        reward.SetActive(true);

        StartCoroutine(BackToMenu(1.5f));
    }

    public void UpdateHud(Ingredient ingredient)
    {
        /*
        if(ingredient != null)
        {
            feedBackText.text = "Proximo ingrediente: " + ingredient.ingredientName;
        }
        else if(RecipeController.usingIngredient)
        {
            feedBackText.text = "Aguardando finalizar preparo";
        }
        else
        {
            feedBackText.text = "Aguardando o primeiro ingrediente";
        }

        finishText.text = "Numero de Recitas acabadas: " + finishText;*/
    }
}
