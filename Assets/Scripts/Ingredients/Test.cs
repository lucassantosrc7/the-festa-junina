﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Test : MonoBehaviour
{
    static bool instaciou = false;
    static Image image;

    [SerializeField]
    Image imageInstantiate;

    int count = 0;

    public void Use()
    {
        if (!instaciou)
        {
            instaciarImage();
        }

        image.gameObject.SetActive(true);
        count = 0;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            count++;
        }

        if(count >= 2)
        {
            image.gameObject.SetActive(false);
            gameObject.SetActive(false);
            count = 0;
            RecipeController.RecipeIsOver(0);
        }
    }

    void instaciarImage()
    {
        image = Instantiate(imageInstantiate, GameController.instance.gameCanvas.transform);
        image.gameObject.SetActive(false);
        instaciou = true;
    }
}
