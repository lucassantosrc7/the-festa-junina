﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Create
{
    public class TrashIngredient : MonoBehaviour
    {
        [HideInInspector]
        public bool finish = false;

        Animator anim;

        void Awake()
        {
            anim = GetComponent<Animator>();
        }

        void OnEnable()
        {
            if(anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0)
            {
                anim.Play(anim.GetCurrentAnimatorStateInfo(0).fullPathHash, 0, 0);
            }
            if (finish)
            {
                finish = false;

                GameController.instance.trashPopup.SetActive(false);
                gameObject.SetActive(false);
            }
        }
    }
}

