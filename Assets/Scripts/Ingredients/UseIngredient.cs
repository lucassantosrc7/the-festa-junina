﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Create
{
    public class UseIngredient : MonoBehaviour
    {
        [HideInInspector]
        public bool finish = false;

        [SerializeField]
        bool game = true;

        Animator anim;

        void Awake()
        {
            anim = GetComponent<Animator>();
        }

        void OnEnable()
        {
            if(anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0)
            {
                anim.Play(anim.GetCurrentAnimatorStateInfo(0).fullPathHash, 0, 0);
            }
            if (finish)
            {
                if (game)
                {
                    RecipeController.RecipeIsOver(0);
                    ControlScreens.instance.ChangeScreen(ControlScreens.Screens.Game);
                }

                SendMessage("initialize", SendMessageOptions.DontRequireReceiver);
                finish = false;
                
                gameObject.SetActive(false);
            }
        }
    }
}

