﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Create;

public class LevelController : Photon.MonoBehaviour
{
    [SerializeField]
    Level [] leveis;

    [SerializeField]
    Button[] buttons;

    [SerializeField]
    Text recipeName;
    [SerializeField]
    GameObject waiting;
    GameObject dadRecipeName;

    [SerializeField]
    Image [] pamonha;

    public static bool porPamonha = false;
    public static List<Recipe> playerRecipes;

    int currentLeveisNum = 0;

    void Start()
    {
        dadRecipeName = recipeName.transform.parent.gameObject;
        dadRecipeName.SetActive(false);

        if(playerRecipes == null)
        {
            playerRecipes = new List<Recipe>();
            for(int i = 0; i < leveis[0].recipes.Length; i++)
            {
                if (!playerRecipes.Contains(leveis[0].recipes[i].recipe))
                {
                    playerRecipes.Add(leveis[0].recipes[i].recipe);
                }
            }
        }

        if (porPamonha)
        {
            for(int i = 0; i < pamonha.Length; i++)
            {
                pamonha[i].color = Color.white;
            }
        }
    }

    void IniLevel()
    {
        waiting.SetActive(false);
        if(dadRecipeName == null)
        {
            dadRecipeName = recipeName.transform.parent.gameObject;
        }
        dadRecipeName.SetActive(false);

        if (!PhotonNetwork.isMasterClient)
        {
            waiting.SetActive(true);
        }
    }

    public void SelectLevel(int posButton)
    {
        int pos = posButton + currentLeveisNum;
        Level level = leveis[pos];
        for (int i = 0; i < level.recipes.Length; i++)
        {
            for(int j = 0; j <= playerRecipes.Count; j++)
            {
                if(j < playerRecipes.Count && playerRecipes[j] == level.recipes[i].recipe)
                {
                    j = playerRecipes.Count + 1;
                }
                else if(j == playerRecipes.Count)
                {
                    dadRecipeName.SetActive(true);
                    recipeName.text = level.recipes[i].recipeName;
                    StartCoroutine(desativePopup(1.8f));
                    return;
                }
            }
        }

        photonView.RPC("LevelSelected", PhotonTargets.All, pos);
    }

    [PunRPC]
    void LevelSelected(int pos)
    {
        GameController.currentLevel = leveis[pos];
        ControlScreens.instance.ChangeScreen(ControlScreens.Screens.Colect);

        RecipeList.recipesList = new GameObject[leveis[pos].recipes.Length];
        for (int i =0; i < leveis[pos].recipes.Length; i++)
        {
            RecipeList.recipesList[i] = leveis[pos].recipes[i].recipe.recipeList;
        }

        GameController.allIngredients = new Dictionary<string, int>();
        GameController.IniAllIngredients = false;
        GameController.ingredients = new Dictionary<string, int>();
        GameController.IniIngredients = false;
    }

    public void Next()
    {
        int x = currentLeveisNum++;
        x += buttons.Length - 1;
        if ( x < leveis.Length)
        {
            currentLeveisNum++;
        }
        else
        {
            if(leveis.Length >= buttons.Length)
            {
                currentLeveisNum = leveis.Length - buttons.Length;
            }
            else
            {
                currentLeveisNum = leveis.Length;
            }
        }
    }

    public void Back()
    {
        currentLeveisNum--;
        if (currentLeveisNum <= 0)
        {
            currentLeveisNum = 0;
        }
    }

    IEnumerator desativePopup(float t)
    {
        yield return new WaitForSeconds(t);
        dadRecipeName.SetActive(false);
    }
}
