﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SelectCharacter;
using Create;
using UnityEngine.UI;

namespace Game
{
    public class FoodTrafficking : Photon.MonoBehaviour
    {
        public static FoodTrafficking Instance { get; private set; }

        Slots slot;

        [Space(10)]
        [Header("PopUps")]
        [SerializeField]
        GameObject send;
        [SerializeField]
        GameObject sendOrUse, bG;

        [Space(10)]
        [Header("Buttons")]
        [SerializeField]
        Button sendButtons;
        [SerializeField]
        Button useButtons;
        [SerializeField]
        ButtonChar[] characters;

        void Awake()
        {
            Instance = this;
            gameObject.SetActive(true);
            send.SetActive(false);
            sendOrUse.SetActive(false);
            bG.SetActive(false);

            for (int i = 0; i < characters.Length; i++)
            {
                ButtonChar b = characters[i];
                if (b.character != GameController.myCharacter)
                {
                    b.button.onClick.AddListener(() => Send(b.character));
                }
                else
                {
                    b.button.gameObject.SetActive(false);
                }
            }
        }

        public void ActivePopup(Slots slot)
        {
            gameObject.SetActive(true);
            sendOrUse.SetActive(true);
            bG.SetActive(true);

            sendButtons.onClick.RemoveAllListeners();
            useButtons.onClick.RemoveAllListeners();

            sendButtons.onClick.AddListener(() => SendSlot(slot));
            useButtons.onClick.AddListener(() => UseSlot(slot));

            useButtons.onClick.AddListener(()  => UseButton(slot));
        }

        public void SendSlot(Slots _slot)
        {
            SendFood(_slot);
        }

        void UseSlot(Slots _slot)
        {
            if (RecipeController.usingIngredient) return;
            RecipeController.CheckRecipe(_slot);
        }

        void SendFood(Slots _slot)
        {
            send.SetActive(true);
            sendOrUse.SetActive(false);
            bG.SetActive(true);
            slot = _slot;
        }

        void UseButton(Slots _slot)
        {
            string iName = _slot.ingredient.ingredientName.ToString();
            photonView.RPC("DecreaseIngredient", PhotonTargets.All, iName, _slot.ingredient.AmountRecipes);

            _slot.amount--;
            _slot.CheckAmount();

            send.SetActive(false);
            sendOrUse.SetActive(false);
            bG.SetActive(false);
        }

        [PunRPC]
        void DecreaseIngredient(string ingredientName, int amount)
        {
            GameController.ingredients[ingredientName] -= amount;
            GameController.instance.CheckAmounIngredients();
        }

        [PunRPC]
        void Receiver(int character, int ingredient)
        {
            if ((int)GameController.myCharacter == character)
            {
                LevelConfig [] recipes = GameController.currentLevel.recipes;
                for (int i = 0; i < recipes.Length; i++)
                {
                    IngredientConfig[] ingredients = recipes[i].recipe.ingredients;
                    for (int j = 0; j < ingredients.Length; j++)
                    {
                        if ((int)ingredients[j].ingredient.ingredientName == ingredient)
                        {
                            GameController.ReceivedAnIngredient(ingredients[j].ingredient);
                        }
                    }
                }
            }
        }

        public void Exit()
        {
            send.SetActive(false);
            sendOrUse.SetActive(false);
            bG.SetActive(false);
        }

        public void Send(GameController.Characters character)
        {
            if (slot != null)
            {
                photonView.RPC("Receiver", PhotonTargets.All, (int)character, (int)slot.ingredient.ingredientName);
                UseButton(slot);
                slot = null;
            }
        }
    }
}
