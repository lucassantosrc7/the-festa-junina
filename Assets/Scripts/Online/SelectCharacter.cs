﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SelectCharacter
{
    public class SelectCharacter : Photon.MonoBehaviour
    {
        [SerializeField]
        ButtonChar[] buttons;

        List<string> availableChars;

        string hasChar = "";
        int playerReady = 0;

        void Start()
        {
            availableChars = new List<string>();
            for (int i = 0; i < buttons.Length; i++)
            {
                ButtonChar b = buttons[i];
                availableChars.Add(b.character.ToString());
                b.button.onClick.AddListener(() => ButtonCharacter(b.character));
            }

            CheckChars();
        }

        void CheckChars()
        {
            if (PhotonNetwork.room == null)
            {
                gameObject.SetActive(false);
                return;
            }

            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].button.gameObject.SetActive(false);
            }

            if (availableChars == null)
            {
                availableChars = new List<string>();
            }

            for (int j = 0; j < availableChars.Count; j++)
            {
                for (int i = 0; i < buttons.Length; i++)
                {
                    ButtonChar b = buttons[i];
                    if (availableChars[j] == b.character.ToString())
                    {
                        buttons[i].button.gameObject.SetActive(true);
                    }
                }
            }
        }

        [PunRPC]
        void RemoveChar(string charName)
        {
            if (availableChars == null)
            {
                availableChars = new List<string>();
            }
            availableChars.Remove(charName);
            CheckChars();

            if(hasChar != "")
            {
                availableChars = new List<string>();
                availableChars.Add(hasChar);
                CheckChars();
            }
        }

        [PunRPC]
        void PlayerReady()
        {
            playerReady++;
            if (playerReady == Launcher.MaxPlayers)
            {
                StartGame();
                hasChar = "";
            }
        }

        public void ButtonCharacter(GameController.Characters character)
        {
            for (int i = 0; i < buttons.Length; i++)
            {
                ButtonChar b = buttons[i];
                if (character == b.character)
                {
                    buttons[i].button.gameObject.SetActive(true);
                }
                else
                {
                    buttons[i].button.gameObject.SetActive(false);
                }
            }

            GameController.myCharacter = character;
            hasChar = character.ToString();
            photonView.RPC("RemoveChar", PhotonTargets.AllBuffered, character.ToString());
            photonView.RPC("PlayerReady", PhotonTargets.AllBuffered);
        }

        public void StartGame()
        {
            ControlScreens.instance.ChangeScreen(ControlScreens.Screens.SelectLevel);
        }
    }

    [System.Serializable]
    public class ButtonChar
    {
        public UnityEngine.UI.Button button;
        public GameController.Characters character;
    }
}