﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Options : MonoBehaviour
{
    public void GoingToMenu()
    {
        ControlScreens.instance.ChangeScreen(ControlScreens.Screens.Menu);
    }

    public void Volume(UnityEngine.UI.Slider slider)
    {
        SoundController.ChangeVolume(slider.value);
    }

    public void GoingToCredits()
    {
        ControlScreens.instance.ChangeScreen(ControlScreens.Screens.Credits);
    }
}
