﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteSound : MonoBehaviour
{

    [SerializeField]
    Image hasSound, hasntSound;

    bool soundMute = false;

    void Start()
    {
        hasntSound.gameObject.SetActive(soundMute);
        hasSound.gameObject.SetActive(!soundMute);

        if (soundMute)
        {
            SoundController.ChangeVolume(0);
        }
        else
        {
            SoundController.ChangeVolume(1);
        }
    }

    public void MuteAudio()
    {
        soundMute = !soundMute;

        if (soundMute)
        {
            SoundController.ChangeVolume(0);
        }
        else
        {
            SoundController.ChangeVolume(1);
        }


        hasntSound.gameObject.SetActive(soundMute);
        hasSound.gameObject.SetActive(!soundMute);
    }
}
