﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Sound", menuName = "Create/Sound", order = 5)]
public class SoundConfig : ScriptableObject
{
    public SoundVariables[] sounds;
}

[System.Serializable]
public class SoundVariables
{
    public string nameClip;
    public AudioClip clip;
}
