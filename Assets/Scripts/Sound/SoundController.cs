﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundController : MonoBehaviour
{
    [SerializeField]
    SoundConfig soundConfigs;
    [SerializeField]
    AudioSource soundAmbience;

    static AudioSource sourceAmbience;
    static AudioSource source;
    public static float volume = 1;
    public static SoundConfig clip;

    void Awake()
    {
        source = GetComponent<AudioSource>();
        sourceAmbience = soundAmbience;

        clip = soundConfigs;
    }

    public static void ChangeVolume(float vol)
    {
        volume = vol;
        sourceAmbience.volume = volume;
        source.volume = volume;
    }

    public void PlaySound(string nameClip)
    {
        Play(nameClip);
    }

    public static void Play(string nameClip)
    {
        if (!source.isPlaying)
        {
            for (int i = 0; i <= clip.sounds.Length; i++)
            {
                if(i < clip.sounds.Length)
                {
                    if (nameClip == clip.sounds[i].nameClip)
                    {
                        source.PlayOneShot(clip.sounds[i].clip, volume);
                        return;
                    }
                }
                else
                {
                    Debug.LogError("Nem um som com esse nome " + nameClip);
                }
            }
        }
    }

    public static void ChangeAmbience(string nameClip)
    {
        for (int i = 0; i < clip.sounds.Length; i++)
        {
            if (i < clip.sounds.Length)
            {
                if (nameClip == clip.sounds[i].nameClip)
                {
                    sourceAmbience.Stop();
                    sourceAmbience.PlayOneShot(clip.sounds[i].clip, volume);
                    return;
                }
            }
            else
            {
                Debug.LogError("Nem um som com esse nome " + nameClip);
            }
        }
    }
}
