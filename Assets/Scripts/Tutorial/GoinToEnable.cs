﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoinToEnable : MonoBehaviour
{
    [SerializeField]
    Tutorial tutorial;

    [SerializeField]
    string step;

    [SerializeField]
    string stepToGo;

    [SerializeField]
    float time = 2;
    float currentTime;

    void Update()
    {
        if(Time.time > currentTime)
        {
            tutorial.GoingToStep(stepToGo);
            currentTime = -1;
        }
    }
    void OnEnable()
    {
        currentTime = -1;
        if(tutorial.currentTutorial.NameTutorial == step)
        {
            currentTime = Time.time + time;
        }
    }
}
