﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauDeSeboTutorial : MonoBehaviour
{
    [SerializeField]
    Tutorial tutorial;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    void OnEnable()
    {
        Time.timeScale = 0;
        tutorial.GoingToStep("Pau de Sebô");
    }
}
