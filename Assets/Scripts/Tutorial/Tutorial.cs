﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    [SerializeField]
    GameObject speakPopup;

    [SerializeField]
    Button nextStepButton;
    Text speech;
    float currentTime;

    [Header("Tutorial Steps")]
    [SerializeField]
    TutorialConfig[] tutorials;

    [HideInInspector]
    public TutorialConfig currentTutorial;

    void Awake()
    {
        speech = speakPopup.GetComponentInChildren<Text>();

        for (int i = 0; i < tutorials.Length; i++)
        {
            tutorials[i].numPos = i;

            if (tutorials[i].objsToActive != null)
            {
                for (int j = 0; j < tutorials[i].objsToActive.Length; j++)
                {
                    tutorials[i].objsToActive[j].SetActive(false);
                }
            }
        }

        currentTutorial = GetTutorialConfig("Ola");
        LetsTalk();
    }

    void Update()
    {
        if(currentTutorial.hasTime && currentTutorial.wasStarted)
        {
            if(Time.time > currentTime)
            {
                speakPopup.SetActive(false);
            }

            if (Time.time > currentTime + 0.3f)
            {
                if (currentTutorial.nextStepTime)
                {
                    NextStep();
                }

                if (currentTutorial.nextStepButton)
                {
                    nextStepButton.gameObject.SetActive(true);
                }
                else
                {
                    nextStepButton.gameObject.SetActive(false);
                }
            }
        }

        if (currentTutorial.activeWithScreen)
        {
            if (ControlScreens.currentScreen == currentTutorial.screen)
            {
                currentTutorial.activeWithScreen = false;
                if (currentTutorial.screen == ControlScreens.Screens.SelectLevel)
                {
                    if (PhotonNetwork.isMasterClient)
                    {
                        GoingToStep("Escolhido");
                    }
                    else
                    {
                        GoingToStep("Não Escolhido");
                    }
                }
                else if(currentTutorial.screen == ControlScreens.Screens.Game 
                     && currentTutorial.NameTutorial == "Música")
                {
                    return;
                }
                else
                {
                    LetsTalk();
                }
            }
            else
            {
                StopTalk();
            }
        }
    }

    public void NextStep()
    {
        StopTalk();
        Time.timeScale = 1;

        int t = currentTutorial.numPos + 1;

        if(t >= tutorials.Length)
        {
            Colect.ColectController.numFishing = 4;
            PhotonNetwork.LeaveRoom();
        }
        else
        {
            currentTutorial = GetTutorialConfig(tutorials[t].NameTutorial);

            LetsTalk();
        }
    }

    public void GoingToStep(string stepName)
    {
        StopTalk();

        for (int i = 0; i < tutorials.Length; i++)
        {
            if (stepName == tutorials[i].NameTutorial)
            {
                currentTutorial = GetTutorialConfig(tutorials[i].NameTutorial);
                LetsTalk();
            }
        }
    }

    void LetsTalk()
    {
        if(currentTutorial.time > 0)
        {
            currentTime = Time.time + currentTutorial.time;
            nextStepButton.gameObject.SetActive(false);
        }
        else
        {
            nextStepButton.gameObject.SetActive(true);
        }

        if (!currentTutorial.nextStepButton)
        {
            nextStepButton.gameObject.SetActive(false);
        }

        if (currentTutorial.objsToActive != null)
        {
            for(int i =0; i < currentTutorial.objsToActive.Length; i++)
            {
                currentTutorial.objsToActive[i].SetActive(true);
            }
        }

        speakPopup.SetActive(true);
        speech.text = currentTutorial.speech;
        currentTutorial.wasStarted = true;
    }

    void StopTalk()
    {
        speakPopup.SetActive(false);
        currentTutorial.wasStarted = false;
        nextStepButton.gameObject.SetActive(false);

        if (currentTutorial.objsToActive != null)
        {
            for (int i = 0; i < currentTutorial.objsToActive.Length; i++)
            {
                currentTutorial.objsToActive[i].SetActive(false);
            }
        }
    }

    TutorialConfig GetTutorialConfig(string nameTutorial)
    {
        for(int i = 0; i < tutorials.Length; i++)
        {
            if(tutorials[i].NameTutorial == nameTutorial)
            {
                return tutorials[i];
            }
        }
        return null;
    }
}

[System.Serializable]
public class TutorialConfig
{
    public string NameTutorial;
    [HideInInspector]
    public int numPos;
    [HideInInspector]
    public bool wasStarted = false;

    [TextArea]
    public string speech;

    [Header("Ways to active")]
    public bool activeWithScreen;
    public ControlScreens.Screens screen;

    [Header("Ways to disable")]
    public bool nextStepButton = true;

    [Header("Time")]
    public bool hasTime = false;
    public bool nextStepTime = false;
    public float time = -1;

    public GameObject[] objsToActive;
}
