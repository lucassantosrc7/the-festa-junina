﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game;

[RequireComponent(typeof(Slots))]
public class TutorialSlot : MonoBehaviour
{
    [SerializeField]
    Create.Ingredient ingredient;

    [SerializeField]
    FoodTrafficking foodTrafficking;

    Slots slot;

    void OnEnable()
    {
        slot = GetComponent<Slots>();

        slot.ingredient = ingredient;
        slot.amount++;

        slot.SendMessage("UpdateSlot");
    }

    public void EnablePopUp()
    {
        foodTrafficking.ActivePopup(slot);
    }
}
